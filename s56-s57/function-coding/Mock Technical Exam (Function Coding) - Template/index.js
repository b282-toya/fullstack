function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }
    let result = 0;
    let lowerCaseSentence = sentence.toLowerCase();
    let lowerCaseLetter = letter.toLowerCase();
    for (let i = 0; i < lowerCaseSentence.length; i++) {
        if (lowerCaseSentence.charAt(i) === lowerCaseLetter) {
            result++;
        }
    }
    return result;
}

function isIsogram(text) {
    text = text.toLowerCase();
    let letters = new Set();
    for (let i = 0; i < text.length; i++) {
        if (letters.has(text.charAt(i))) {
            return false;
        }
        letters.add(text.charAt(i));
    }
    return true;
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    }
    let finalPrice = price;
    if ((age >= 13 && age <= 21) || age >= 65) {
        finalPrice = price * 0.8;
    }
    return finalPrice.toFixed(2);
}




function findHotCategories(items) {
    let hotCategories = [];
    for (let i = 0; i < items.length; i++) {
        if (items[i].stocks === 0 && !hotCategories.includes(items[i].category)) {
            hotCategories.push(items[i].category);
        }
    }
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    const votedForA = new Set(candidateA);
    const votedForB = new Set(candidateB);

    const flyingVoters = [];

    for (const voter of votedForA) {
        if (votedForB.has(voter)) {
            flyingVoters.push(voter);
        }
    }

    return flyingVoters;
}


console.log(countLetter('a', 'This is an example sentence with some As.')); // Output: 3
console.log(isIsogram('Machine')); // Output: true
console.log(purchase(25, 99.99)); // Output: "100"
console.log(findHotCategories([
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
])); // Output: ['toiletries', 'gadgets']
console.log(findFlyingVoters(
    ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'],
    ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
)); // Output: ['LIWf1l', 'V2hjZH']


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};