import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';

import Home from './pages/Home.js';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';


import './App.css';

import { UserProvider } from './UserContext';

import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';

function App() {

  // const [user, setUser] = useState({email: localStorage.getItem('email')});

  const [ user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  // Used to check if the user information is properly stored upon login in the localStorage and cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // if user is logged in
      if(typeof data._id !== 'undefined') {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // if user is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, [])


  return (
    <>
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/courses' element={<Courses />} />
            <Route path='/courses/:courseId' element={<CourseView />} />
            <Route path='/register' element={<Register />} />
            <Route path='/login' element={<Login />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/*' element={<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
   </> 
  );
}

export default App;