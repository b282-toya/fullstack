import {Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({course}) {
    //destructure
    const {name, description, price, _id} = course;

    /*
    SYNTAX:
        const [getter, setter] = useState(initialGetterValue);
    */
// [SECTION] ACTIVITY S51

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);
    // console.log(useState(0));

   // function enroll() {
   //  if (seats === 0) {
   //      alert("No more seats");
   //      return;
   //  }
   //  setCount(count + 1);
   //  setSeats(seats - 1);
// }

// function enroll() {
//     if (seats > 0) {
//         setCount(count + 1);
//         setSeats(seats - 1);
//     } else {
//         alert("No more seats available");
//     };
// }


// [END] Activity

// [SECTION] s52 TOPIC START

//     const [count, setCount] = useState(0);
//     const [seats, setSeats] = useState(5);

// function enroll() {
//     setCount(count + 1);
//     setSeats(seats - 1);
// }

// useEffect(() => {
//     // conditional statements
//     if(seats <= 0) {
//         alert("No more seats available")
//     };
//     // [seats] = dependency/ies
//     // without dependecy = for log-outs
//     // [] = 1 time run
//     // gagamit ng dependency kapag meron minomonitor na changes sa conditional statement
// }, [seats]);






return (
    // <Row className="mt-3 mb-3">
    //         <Col xs={12}>
    //             <Card className="cardHighlight p-0">
    //                 <Card.Body>
    //                     <Card.Title><h4>{name}</h4></Card.Title>
    //                     <Card.Subtitle>Description</Card.Subtitle>
    //                     <Card.Text>{description}</Card.Text>
    //                     <Card.Subtitle>Price</Card.Subtitle>
    //                     <Card.Text>{price}</Card.Text>
    //                     <Card.Subtitle>Enrollees</Card.Subtitle>
    //                     <Card.Text>{count} Enrollees</Card.Text>

    //                     <Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>
    //                 </Card.Body>
    //             </Card>
    //         </Col>
    // </Row>        

    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0">
                <Card.Body>
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row> 

    )
}

// in button, disabled={seats<=0} [s52]