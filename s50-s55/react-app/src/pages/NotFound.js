// import { useNavigate } from 'react-router-dom'
// import { Button } from 'react-bootstrap'

// export default function NotFound(){
// 	const navigate = useNavigate()

// 	return(
// 		<>
// 			<h1>Error 404 - Page not found</h1>
// 			<p> The page you are looking for cannot be found </p>
// 			<Button variant="primary" onClick={() => navigate(-1)}>Back to Home</Button>
// 		</>
// 	)
// }


import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} />
    )
}