import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);

	// clears the user state
	unsetUser();

	// clears the user info after log-out
	useEffect(() => {
		setUser({id: null})
	});

	// localStorage.clear();

	return(
		<Navigate to='/login' />
	)
}