import React from 'react'

// to create content and pass the data/info to multiple components w/o using props
const UserContext = React.createContext()

// for providing the data/state to all components (supplier)
export const UserProvider = UserContext.Provider 

export default UserContext
