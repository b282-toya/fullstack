/*
============================
Creating react application
============================

Terminal > npx create-react-app project-name
// allows us to create a react application that uses a "toolchain" that downloads a number of files/folders that make up our basic react application.

Delete unnecessary files
App.test.js
index.css
logo.svg
reportWebVitals.js

Follow the errors to identify what imports need to be deleted.

============================
Babel Linting
============================

JavaScript (Babel) - Linting for code readability

IOS
Sublime > Cmd + Shift + P

Linux and Windows
Sublime > Ctrl + Shift + P

- In the input field type the word "install"
- Select the "Package Control: Install Package"
- Type "babel" in the input field to search for the "Babel" linting to be installed
- Change the linting of the sublime text editor to "Javascript(Babel)

============================
React-BootStrap & Bootstrap
============================

Terminal > npm install react-bootstrap bootstrap
The "react-bootstrap" package allows us to gain access to ready made React JS components similiar to Bootstrap components.
The "bootstrap" package is also installed within our application to allow us access to Bootstrap classes that we can utilize to rapidly create an application.


============================
S50 NOTES
============================
React JS Components are independent, reusable pieces of code which normally contain JavaScript and JSX syntax which make up a part of our application.

The naming convention for React JS components follows the "Pascal Case" having capitalized letters for all words of the function name AND the file name associated with it.

"export default" 
	The "export default" statements allow us to create a JavaScript module that will be used when the file is exported in a different component.

React JS is a single page application (SPA)
	Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components

"return"
	The "return" statement in a React JS component is what defines what will be rendered/displayed in our application.

<></>
	The "Fragment" component ensures that this error can be prevented.
	Common pattern in React is for a component to return multiple elements.

React.StrictMode
	The "React.StrictMode" component is a tool for highlighting potential problems in an application and provide more information regarding the errors encountered.

============================
S51 NOTES
============================
Props
	Props in React.js, short for properties, Props are the information that a component receives, usually from a parent component.
	Example:
		CourseCard courseProp = {coursesData[0]}
		CourseCard key={course.id} course = {course}

Prop Drilling
	We can pass information from one component to another using props. This is referred to as "props drilling"

{} in Props
	The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ("")
	

States
	States in React.js allow components to create manage its own data and is meant to be used internally.


Hooks
	Hooks in React.js are functions that allow developers to create and manage states and lifecycle within components. 

useState()
	const [getter, setter] = useState(initialGetterValue);
		React Hook that lets you add a state variable to the component.
		Use for storing states
