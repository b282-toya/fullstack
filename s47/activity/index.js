
const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector(".span-full-name");

let updateFullNameSpan = () => {
	let first_name = txtFirstName.value;
	let last_name = txtLastName.value;

	spanFullName.innerHTML = `${first_name} ${last_name}`;
};

txtFirstName.addEventListener('keyup', updateFullNameSpan);

txtLastName.addEventListener('keyup', updateFullNameSpan);
