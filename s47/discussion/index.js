// [DISCUSSION]
const txtFirstName = document.querySelector("#txt-first-name")
const spanFullName = document.querySelector(".span-full-name")
// [ACTIVITY]
const txtLastName = document.querySelector("#txt-last-name")



// Alternatively, we can use the getElement functions to retrieve elements
// document.getElementById
// document.getElementByClassName
// document.getElementByTagName
// const txtFirstName = document.getElementById("txt-first-name");
// const spanFullName = document.getElementById("span-full-name");

// Whenever a user interacts with a webpage, this action is considered as an event
// "addEventListener" is a function that takes 2 arguments
// "keydown" is a string identifying an event
// Second argument - will execute once the specified event is triggered
// .value = gets the value of the input object from the document
// txtFirstName.addEventListener('keyup', (event) => {
// "innerHTML" property that sets or returns the HTML content
// 	spanFullName.innerHTML = txtFirstName.value;

// e = event
// .target = contains element where the event is happening
// .value = gets the value of the input object
// 	console.log(event.target);
// 	console.log(event.target.value);
// });


let updateFullNameSpan = () => {
	let first_name = txtFirstName.value
	let last_name = txtLastName.value 

	spanFullName.innerHTML = `${first_name} ${last_name}`
};


txtFirstName.addEventListener('keyup', updateFullNameSpan)
txtLastName.addEventListener('keyup', updateFullNameSpan)
